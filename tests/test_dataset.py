import json
import os.path
import unittest
import uuid

from sklearn import datasets

from d3m import utils
from d3m.container import dataset
from d3m.metadata import base as metadata_base


def convert_metadata(metadata):
    return json.loads(json.dumps(metadata, cls=utils.JsonEncoder))


class TestDataset(unittest.TestCase):
    def test_d3m(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        ds = dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        self._test_d3m(ds, dataset_doc_path)

    def _test_d3m(self, ds, dataset_doc_path):
        ds.metadata.check(ds)

        for row in ds['learningData']:
            for cell in row:
                # Nothing should be parsed from a string.
                self.assertIsInstance(cell, str)

        self.assertEqual(len(ds['learningData']), 150)
        self.assertEqual(len(ds['learningData'].iloc[0]), 6)

        self.maxDiff = None

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': 'iris_dataset_1',
            'version': '1.0',
            'name': 'Iris Dataset',
            'location_uris': [
                'file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path),
            ],
            'source': {
                'license': 'CC',
                'redacted': False,
                'human_subjects_research': False,
            },
            'dimension': {
                'length': 1,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
            'digest': 'b5cda6740bae31be252316e877773a62307c44b13209af5eab30321e1a28df84',
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData',))), {
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
                'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 150,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 6,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'd3mIndex',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })

        for i in range(1, 5):
            self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, i))), {
                'name': ['sepalLength', 'sepalWidth', 'petalLength', 'petalWidth'][i - 1],
                'structural_type': 'str',
                'semantic_types': [
                    'http://schema.org/Float',
                    'https://metadata.datadrivendiscovery.org/types/Attribute',
                ],
            })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 5))), {
            'name': 'species',
            'structural_type': 'str',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/CategoricalData',
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
            ],
        })

    def test_d3m_lazy(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        ds = dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path), lazy=True)

        ds.metadata.check(ds)

        self.assertTrue(len(ds) == 0)
        self.assertTrue(ds.is_lazy())

        self.maxDiff = None

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': 'iris_dataset_1',
            'version': '1.0',
            'name': 'Iris Dataset',
            'location_uris': [
                'file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path),
            ],
            'source': {
                'license': 'CC',
                'redacted': False,
                'human_subjects_research': False,
            },
            'dimension': {
                'length': 0,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
            'digest': 'b5cda6740bae31be252316e877773a62307c44b13209af5eab30321e1a28df84',
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData',))), {})
        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS))), {})
        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 0))), {})

        ds.load_lazy()

        self.assertFalse(ds.is_lazy())

        self._test_d3m(ds, dataset_doc_path)

    def test_csv(self):
        dataset_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'tables', 'learningData.csv'))

        dataset_id = '219a5e7b-4499-4160-9b72-9cfa53c4924d'
        dataset_name = 'Iris Dataset'

        ds = dataset.Dataset.load('file://{dataset_path}'.format(dataset_path=dataset_path), dataset_id=dataset_id, dataset_name=dataset_name)

        self._test_csv(ds, dataset_path, dataset_id, dataset_name)

    def _test_csv(self, ds, dataset_path, dataset_id, dataset_name):
        ds.metadata.check(ds)

        for row in ds['learningData']:
            for cell in row:
                # Nothing should be parsed from a string.
                self.assertIsInstance(cell, str)

        self.assertEqual(len(ds['learningData']), 150)
        self.assertEqual(len(ds['learningData'].iloc[0]), 6)

        self.maxDiff = None

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': dataset_id,
            'name': dataset_name,
            'stored_size': 4961,
            'location_uris': [
                'file://localhost{dataset_path}'.format(dataset_path=dataset_path),
            ],
            'dimension': {
                'length': 1,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
            'digest': 'a5e827f2fb60639f1eb7b9bd3b849b0db9c308ba74d0479c20aaeaad77ccda48',
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData',))), {
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
                'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 150,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 6,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'd3mIndex',
            'structural_type': 'str',
        })

        for i in range(1, 5):
            self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, i))), {
                'name': ['sepalLength', 'sepalWidth', 'petalLength', 'petalWidth'][i - 1],
                'structural_type': 'str',
            })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 5))), {
            'name': 'species',
            'structural_type': 'str',
        })

    def test_csv_lazy(self):
        dataset_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'tables', 'learningData.csv'))

        dataset_id = '219a5e7b-4499-4160-9b72-9cfa53c4924d'
        dataset_name = 'Iris Dataset'

        ds = dataset.Dataset.load('file://{dataset_path}'.format(dataset_path=dataset_path), dataset_id=dataset_id, dataset_name=dataset_name, lazy=True)

        ds.metadata.check(ds)

        self.assertTrue(len(ds) == 0)
        self.assertTrue(ds.is_lazy())

        self.maxDiff = None

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': dataset_id,
            'name': dataset_name,
            'location_uris': [
                'file://localhost{dataset_path}'.format(dataset_path=dataset_path),
            ],
            'dimension': {
                'length': 0,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData',))), {})
        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS))), {})
        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 0))), {})

        ds.load_lazy()

        self.assertFalse(ds.is_lazy())

        self._test_csv(ds, dataset_path, dataset_id, dataset_name)

    def test_sklearn(self):
        for dataset_path in ['boston', 'breast_cancer', 'diabetes', 'digits', 'iris', 'linnerud']:
            dataset.Dataset.load('sklearn://{dataset_path}'.format(dataset_path=dataset_path))

        dataset_uri = 'sklearn://iris'
        dataset_id = str(uuid.uuid3(uuid.NAMESPACE_URL, dataset_uri))
        dataset_name = 'Iris Dataset'

        ds = dataset.Dataset.load(dataset_uri, dataset_id=dataset_id, dataset_name=dataset_name)

        self._test_sklearn(ds, dataset_uri)

    def _test_sklearn(self, ds, dataset_uri):
        ds.metadata.check(ds)

        self.assertEqual(len(ds['learningData']), 150)
        self.assertEqual(len(ds['learningData'].iloc[0]), 5)

        self.maxDiff = None

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': '44f6efaa-72e7-383e-9369-64bd7168fb26',
            'name': 'Iris Dataset',
            'location_uris': [
                dataset_uri,
            ],
            'description': datasets.load_iris()['DESCR'],
            'dimension': {
                'length': 1,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
            'digest': '2cd0dd490ba383fe08a9f89514f6688bb5cb77d4a7da140e9458e7c534eb82f4',
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData',))), {
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
                'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 150,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 5,
            }
        })

        for i in range(0, 4):
            self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, i))), {
                'name': ['sepal length (cm)', 'sepal width (cm)', 'petal length (cm)', 'petal width (cm)'][i],
                'structural_type': 'numpy.float64',
                'semantic_types': [
                    'https://metadata.datadrivendiscovery.org/types/Attribute',
                ],
            })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 4))), {
            'name': 'column 4',
            'structural_type': 'str',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
                'https://metadata.datadrivendiscovery.org/types/CategoricalData',
            ],
        })

    @unittest.skip("requires rewrite")
    # TODO: Fix. Currently "set_for_value" is not called when not loading lazily.
    #       We should just always use auto generation for as much as possible.
    def test_sklearn_lazy(self):
        for dataset_path in ['boston', 'breast_cancer', 'diabetes', 'digits', 'iris', 'linnerud']:
            dataset.Dataset.load('sklearn://{dataset_path}'.format(dataset_path=dataset_path))

        dataset_uri = 'sklearn://iris'
        dataset_id = str(uuid.uuid3(uuid.NAMESPACE_URL, dataset_uri))
        dataset_name = 'Iris Dataset'

        ds = dataset.Dataset.load(dataset_uri, dataset_id=dataset_id, dataset_name=dataset_name, lazy=True)

        ds.metadata.check(ds)

        self.assertTrue(len(ds) == 0)
        self.assertTrue(ds.is_lazy())

        self.maxDiff = None

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': '44f6efaa-72e7-383e-9369-64bd7168fb26',
            'name': 'Iris Dataset',
            'location_uris': [
                dataset_uri,
            ],
            'dimension': {
                'length': 0,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData',))), {})
        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS))), {})
        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 0))), {})

        ds.load_lazy()

        self.assertFalse(ds.is_lazy())

        self._test_sklearn(ds, dataset_uri)

    def test_multi_table(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))

        dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

    def test_timeseries(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'timeseries_dataset_1', 'datasetDoc.json'))

        ds = dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        self.assertEqual(ds.metadata.query(('0', metadata_base.ALL_ELEMENTS, 0))['file_columns'], ({
            'semantic_types': ('http://schema.org/DateTime', 'https://metadata.datadrivendiscovery.org/types/PrimaryKey'),
            'name': 'Date',
        }, {
            'semantic_types': ('http://schema.org/Float', 'https://metadata.datadrivendiscovery.org/types/Attribute'),
            'name': 'Close',
        }))

    def test_audio(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'audio_dataset_1', 'datasetDoc.json'))

        ds = dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        self.maxDiff = None

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': 'audio_dataset_1',
            'version': '1.0',
            'name': 'Audio dataset to be used for tests',
            'location_uris': [
                'file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path),
            ],
            'source': {
                'license': 'CC0',
                'redacted': False,
                'human_subjects_research': False,
            },
            'dimension': {
                'length': 2,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
            'digest': '2e1ca903305f0e71f8b6ab5f81d942e0335e6fe5ae901b311b1bcd1aed9a5fe1',
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('0',))), {
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
                'https://metadata.datadrivendiscovery.org/types/FilesCollection',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 1,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('0', metadata_base.ALL_ELEMENTS))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 1,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData',))), {
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
                'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 1,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 5,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'd3mIndex',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 1))), {
            'name': 'audio_file',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Text',
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
            'foreign_key': {
                'type': 'COLUMN',
                'resource_id': '0',
                'column_index': 0,
            },
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 2))), {
            'name': 'start',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Float',
                'https://metadata.datadrivendiscovery.org/types/Boundary',
                'https://metadata.datadrivendiscovery.org/types/IntervalStart',
            ],
            'boundary_for': {
                'resource_id': 'learningData',
                'column_index': 1,
            },
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 3))), {
            'name': 'end',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Float',
                'https://metadata.datadrivendiscovery.org/types/Boundary',
                'https://metadata.datadrivendiscovery.org/types/IntervalEnd',
            ],
            'boundary_for': {
                'resource_id': 'learningData',
                'column_index': 1,
            },
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 4))), {
            'name': 'class',
            'structural_type': 'str',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/CategoricalData',
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
            ],
        })

    def test_raw(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'raw_dataset_1', 'datasetDoc.json'))

        ds = dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': 'https://metadata.datadrivendiscovery.org/schemas/v0/container.json',
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': 'raw_dataset_1',
            'version': '1.0',
            'name': 'Raw dataset to be used for tests',
            'location_uris': [
                'file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path),
            ],
            'dimension': {
                'name': 'resources',
                'semantic_types': [
                    'https://metadata.datadrivendiscovery.org/types/DatasetResource'
                ],
                'length': 1
            },
            'digest': '82a8bf65357056ad16a611c9ff439061364c09a69d10240a5eaec1927ae6b974',
            'source': {
                'license': '',
                'redacted': False,
                'human_subjects_research': False
            }
        })
        self.assertEqual(convert_metadata(ds.metadata.query(('0', metadata_base.ALL_ELEMENTS, 0))), {
            'location_base_uris': [
                'file://{dataset_path}/raw/'.format(dataset_path=os.path.dirname(dataset_doc_path)),
            ],
            'media_types': ['text/csv'],
            'name': 'filename',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
                'https://metadata.datadrivendiscovery.org/types/FileName',
                'https://metadata.datadrivendiscovery.org/types/UnspecifiedStructure',
            ],
            'structural_type': 'str',
        })


if __name__ == '__main__':
    unittest.main()
