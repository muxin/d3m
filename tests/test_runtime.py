import collections
import os
import pandas
import pickle
import sys
import typing
import unittest

from d3m import container, index, runtime, utils
from d3m.metadata import base as metadata_base, hyperparams, pipeline
from d3m.primitive_interfaces import base, transformer

TEST_PRIMITIVES_DIR = os.path.join(os.path.dirname(__file__), 'data', 'primitives')

sys.path.insert(0, TEST_PRIMITIVES_DIR)

from test_primitives.monomial import MonomialPrimitive
from test_primitives.random import RandomPrimitive
from test_primitives.sum import SumPrimitive
from test_primitives.increment import IncrementPrimitive
from test_primitives.primitive_sum import PrimitiveSumPrimitive
from test_primitives.null import NullPrimitive


TEST_PIPELINE_1 = """
{
    "context": "TESTING",
    "created": "2018-11-05T04:14:02.720699Z",
    "id": "3ffcc6a0-313e-44ae-b551-2ade1386c11e",
    "inputs": [
        {
            "name": "inputs1"
        },
        {
            "name": "inputs2"
        },
        {
            "name": "inputs3"
        }
    ],
    "outputs": [
        {
            "data": "steps.1.produce",
            "name": "Metafeatures"
        }
    ],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/pipeline.json",
    "steps": [
        {
            "arguments": {
                "inputs": {
                    "data": [
                        "inputs.0",
                        "inputs.1",
                        "inputs.2"
                    ],
                    "type": "CONTAINER"
                }
            },
            "outputs": [
                {
                    "id": "produce"
                }
            ],
            "primitive": {
                "id": "8a8a8c15-bb69-488e-834c-f129de2dd2f6",
                "name": "Vertical Concatenate Primitive",
                "python_path": "d3m.primitives.data_transformation.vertical_concatenate.Test",
                "version": "0.1.0"
            },
            "type": "PRIMITIVE"
        },
        {
            "arguments": {
                "inputs": {
                    "data": "steps.0.produce",
                    "type": "CONTAINER"
                }
            },
            "outputs": [
                {
                    "id": "produce"
                }
            ],
            "primitive": {
                "id": "aea7fc39-f40b-43ce-b926-89758e560e50",
                "name": "Voting Primitive",
                "python_path": "d3m.primitives.classification.voting.Test",
                "version": "0.1.0"
            },
            "type": "PRIMITIVE"
        }
    ]
}
"""


class Resolver(pipeline.Resolver):
    def _get_primitive(self, primitive_description: typing.Dict) -> typing.Optional[typing.Type[base.PrimitiveBase]]:
        # To hide any logging or stdout output.
        with utils.silence():
            return super()._get_primitive(primitive_description)


class Hyperparams(hyperparams.Hyperparams):
    pass


DataFramesInputs = container.List
DataFrameOutputs = container.DataFrame


class VerticalConcatenatePrimitive(transformer.TransformerPrimitiveBase[DataFramesInputs, DataFrameOutputs, Hyperparams]):
    """Description."""

    metadata = metadata_base.PrimitiveMetadata({
        'id': '8a8a8c15-bb69-488e-834c-f129de2dd2f6',
        'version': '0.1.0',
        'name': "Vertical Concatenate Primitive",
        'python_path': 'd3m.primitives.data_transformation.vertical_concatenate.Test',
        'algorithm_types': [
            metadata_base.PrimitiveAlgorithmType.ARRAY_CONCATENATION,
        ],
        'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION
    })

    def produce(self, *, inputs: DataFramesInputs, timeout: float = None, iterations: int = None) -> base.CallResult[DataFrameOutputs]:
        for i in range(len(inputs)):
            if not inputs.metadata.has_semantic_type((i, metadata_base.ALL_ELEMENTS, 1), 'https://metadata.datadrivendiscovery.org/types/PredictedTarget'):
                raise Exception("Required metadata missing.")

        outputs = pandas.concat(inputs, ignore_index=True)
        outputs.metadata = outputs.metadata.clear(generate_metadata=True)
        return base.CallResult(outputs)


VotingInputs = container.DataFrame
VotingOutputs = container.DataFrame


class VotingPrimitive(transformer.TransformerPrimitiveBase[VotingInputs, VotingOutputs, Hyperparams]):
    """Description."""

    metadata = metadata_base.PrimitiveMetadata({
        'id': 'aea7fc39-f40b-43ce-b926-89758e560e50',
        'version': '0.1.0',
        'name': "Voting Primitive",
        'python_path': 'd3m.primitives.classification.voting.Test',
        'algorithm_types': [
            metadata_base.PrimitiveAlgorithmType.AGGREGATE_FUNCTION,
        ],
        'primitive_family': metadata_base.PrimitiveFamily.CLASSIFICATION
    })

    def produce(self, *, inputs: VotingInputs, timeout: float = None, iterations: int = None) -> base.CallResult[VotingOutputs]:
        result = inputs.groupby('d3mIndex').apply(lambda x: x['class'].mode())
        result.columns = ['class']
        result = result.reset_index()
        return base.CallResult(container.DataFrame(result))


class TestRuntime(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # To hide any logging or stdout output.
        with utils.silence():
            index.register_primitive('d3m.primitives.regression.monomial.Test', MonomialPrimitive)
            index.register_primitive('d3m.primitives.data_generation.random.Test', RandomPrimitive)
            index.register_primitive('d3m.primitives.operator.sum.Test', SumPrimitive)
            index.register_primitive('d3m.primitives.operator.increment.Test', IncrementPrimitive)
            index.register_primitive('d3m.primitives.operator.primitive_sum.Test', PrimitiveSumPrimitive)
            index.register_primitive('d3m.primitives.operator.null.Test', NullPrimitive)
            index.register_primitive('d3m.primitives.classification.voting.Test', VotingPrimitive)
            index.register_primitive('d3m.primitives.data_transformation.vertical_concatenate.Test', VerticalConcatenatePrimitive)

    def test_basic(self):
        with open(os.path.join(os.path.dirname(__file__), 'data', 'pipelines', 'random-sample.yml'), 'r') as pipeline_file:
            p = pipeline.Pipeline.from_yaml(pipeline_file, resolver=Resolver())

        r = runtime.Runtime(p)

        inputs = [container.List([0, 1, 42])]

        result = r.fit(inputs, return_values=['outputs.0'])
        result.check_success()

        self.assertEqual(len(result.values), 1)

        dataframe = result.values['outputs.0']

        self.assertEqual(dataframe.values.tolist(), [
            [1.764052345967664 + 1],
            [0.4001572083672233 + 1],
            [-1.7062701906250126 + 1],
        ])

        result = r.produce(inputs, return_values=['outputs.0'])
        result.check_success()

        self.assertEqual(len(result.values), 1)

        dataframe = result.values['outputs.0']

        self.assertEqual(dataframe.values.tolist(), [
            [1.764052345967664 + 1],
            [0.4001572083672233 + 1],
            [-1.7062701906250126 + 1],
        ])

        pickled = pickle.dumps(r)
        restored = pickle.loads(pickled)

        result = restored.produce(inputs, return_values=['outputs.0'])
        result.check_success()

        self.assertEqual(len(result.values), 1)

        dataframe = result.values['outputs.0']

        self.assertEqual(dataframe.values.tolist(), [
            [1.764052345967664 + 1],
            [0.4001572083672233 + 1],
            [-1.7062701906250126 + 1],
        ])

        pickle.dumps(r)

        r = runtime.Runtime(p, random_seed=42)

        inputs = [container.List([0, 1, 42])]

        result = r.fit(inputs, return_values=['outputs.0'])
        result.check_success()

        self.assertEqual(len(result.values), 1)

        dataframe = result.values['outputs.0']

        self.assertEqual(dataframe.values.tolist(), [
            [0.4967141530112327 + 1],
            [-0.13826430117118466 + 1],
            [-0.11564828238824053 + 1],
        ])

        pickle.dumps(r)

        r = runtime.Runtime(p, [{}, {'amount': 10}], random_seed=42)

        inputs = [container.List([0, 1, 42])]

        result = r.fit(inputs, return_values=['outputs.0'])
        result.check_success()

        self.assertEqual(len(result.values), 1)

        dataframe = result.values['outputs.0']

        self.assertEqual(dataframe.values.tolist(), [
            [0.4967141530112327 + 10],
            [-0.13826430117118466 + 10],
            [-0.11564828238824053 + 10],
        ])

        pickle.dumps(r)

    def test_argument_list(self):
        p = pipeline.Pipeline.from_json(TEST_PIPELINE_1, resolver=Resolver())

        r = runtime.Runtime(p)

        inputs = [
            container.DataFrame({'d3mIndex': [1, 2, 3], 'class': [0, 0, 0]}),
            container.DataFrame({'d3mIndex': [1, 2, 3], 'class': [0, 0, 1]}),
            container.DataFrame({'d3mIndex': [1, 2, 3], 'class': [0, 1, 1]}),
        ]

        for df in inputs:
            df.metadata = df.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 1), 'https://metadata.datadrivendiscovery.org/types/PredictedTarget')

        result = r.fit(inputs, return_values=['outputs.0'])
        result.check_success()
        dataframe = result.values['outputs.0']

        self.assertEqual(dataframe.values.tolist(), [[1, 0], [2, 0], [3, 1]])

        pickle.dumps(r)

    def test_pipeline_with_primitives_as_hyperparams_from_pipeline(self):
        # We create the pipeline.
        pipeline_description = pipeline.Pipeline(context=pipeline.PipelineContext.TESTING)
        pipeline_description.add_input(name='input_0')
        pipeline_description.add_input(name='input_1')

        step_0_primitive = index.get_primitive('d3m.primitives.regression.monomial.Test')
        step_0_primitive_metadata = step_0_primitive.metadata.query()
        step_0_primitive_description = {
            'id': step_0_primitive_metadata['id'],
            'version': step_0_primitive_metadata['version'],
            'python_path': step_0_primitive_metadata['python_path'],
            'name': step_0_primitive_metadata['name'],
            'digest': step_0_primitive_metadata['digest'],
        }

        step_0 = pipeline.PrimitiveStep(primitive_description=step_0_primitive_description)
        step_0.add_argument(name='inputs', argument_type=pipeline.ArgumentType.CONTAINER, data_reference='inputs.0')
        step_0.add_argument(name='outputs', argument_type=pipeline.ArgumentType.CONTAINER, data_reference='inputs.1')
        step_0.add_output('produce')
        pipeline_description.add_step(step_0)

        step_1_primitive = index.get_primitive('d3m.primitives.operator.primitive_sum.Test')
        step_1_primitive_metadata = step_1_primitive.metadata.query()
        step_1_primitive_description = {
            'id': step_1_primitive_metadata['id'],
            'version': step_1_primitive_metadata['version'],
            'python_path': step_1_primitive_metadata['python_path'],
            'name': step_1_primitive_metadata['name'],
            'digest': step_1_primitive_metadata['digest'],
        }

        step_1 = pipeline.PrimitiveStep(primitive_description=step_1_primitive_description)
        step_1.add_argument(name='inputs', argument_type=pipeline.ArgumentType.CONTAINER, data_reference='inputs.0')
        step_1.add_hyperparameter(name='primitive_1', argument_type=pipeline.ArgumentType.PRIMITIVE, data=0)
        step_1.add_hyperparameter(name='primitive_2', argument_type=pipeline.ArgumentType.PRIMITIVE, data=0)
        step_1.add_output('produce')
        pipeline_description.add_step(step_1)

        pipeline_description.add_output(name='output', data_reference='steps.1.produce')

        r = runtime.Runtime(pipeline_description)

        inputs = [container.List([1, 2, 3, 4, 5]), container.List([2, 4, 6, 8, 100])]

        result = r.fit(inputs, return_values=['outputs.0'])
        result.check_success()

        self.assertEqual(len(result.values), 1)

        results = result.values['outputs.0']

        self.assertEqual(results, [
            11.2,
            22.4,
            33.599999999999994,
            44.8,
            56.0,
        ])

        result = r.produce(inputs, return_values=['outputs.0'])
        result.check_success()

        self.assertEqual(len(result.values), 1)

        results = result.values['outputs.0']

        self.assertEqual(results, [
            11.2,
            22.4,
            33.599999999999994,
            44.8,
            56.0,
        ])

        # Primitive should not be the same instance.
        self.assertIsNot(r.steps_state[0], r.steps_state[1].hyperparams['primitive_1'])
        self.assertIsNot(r.steps_state[1].hyperparams['primitive_1'], r.steps_state[1].hyperparams['primitive_2'])

        pickle._dumps(r)

    def test_pipeline_with_primitives_as_hyperparams_as_class_value(self):
        # We create the pipeline.
        pipeline_description = pipeline.Pipeline(context=pipeline.PipelineContext.TESTING)
        pipeline_description.add_input(name='input_0')

        null_primitive = index.get_primitive('d3m.primitives.operator.null.Test')

        step_0_primitive = index.get_primitive('d3m.primitives.operator.primitive_sum.Test')
        step_0_primitive_metadata = step_0_primitive.metadata.query()
        step_0_primitive_description = {
            'id': step_0_primitive_metadata['id'],
            'version': step_0_primitive_metadata['version'],
            'python_path': step_0_primitive_metadata['python_path'],
            'name': step_0_primitive_metadata['name'],
            'digest': step_0_primitive_metadata['digest'],
        }

        step_0 = pipeline.PrimitiveStep(primitive_description=step_0_primitive_description)
        step_0.add_argument(name='inputs', argument_type=pipeline.ArgumentType.CONTAINER, data_reference='inputs.0')
        step_0.add_hyperparameter(name='primitive_1', argument_type=pipeline.ArgumentType.VALUE, data=null_primitive)
        step_0.add_hyperparameter(name='primitive_2', argument_type=pipeline.ArgumentType.VALUE, data=null_primitive)
        step_0.add_output('produce')
        pipeline_description.add_step(step_0)

        pipeline_description.add_output(name='output', data_reference='steps.0.produce')

        r = runtime.Runtime(pipeline_description)

        inputs = [container.List([1, 2, 3, 4, 5])]

        result = r.fit(inputs, return_values=['outputs.0'])
        result.check_success()

        self.assertEqual(len(result.values), 1)

        results = result.values['outputs.0']

        self.assertEqual(results, [
            2, 4, 6, 8, 10,
        ])

        result = r.produce(inputs, return_values=['outputs.0'])
        result.check_success()

        self.assertEqual(len(result.values), 1)

        results = result.values['outputs.0']

        self.assertEqual(results, [
            2, 4, 6, 8, 10,
        ])

        # Primitive should not be the same instance.
        self.assertIsNot(r.steps_state[0].hyperparams['primitive_1'], r.steps_state[0].hyperparams['primitive_2'])

        pickle.dumps(r)

    def test_pipeline_with_primitives_as_hyperparams_as_instance_value(self):
        # We create the pipeline.
        pipeline_description = pipeline.Pipeline(context=pipeline.PipelineContext.TESTING)
        pipeline_description.add_input(name='input_0')

        null_primitive = index.get_primitive('d3m.primitives.operator.null.Test')

        hyperparams_class = null_primitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        primitive = null_primitive(hyperparams=hyperparams_class.defaults())

        step_0_primitive = index.get_primitive('d3m.primitives.operator.primitive_sum.Test')
        step_0_primitive_metadata = step_0_primitive.metadata.query()
        step_0_primitive_description = {
            'id': step_0_primitive_metadata['id'],
            'version': step_0_primitive_metadata['version'],
            'python_path': step_0_primitive_metadata['python_path'],
            'name': step_0_primitive_metadata['name'],
            'digest': step_0_primitive_metadata['digest'],
        }

        step_0 = pipeline.PrimitiveStep(primitive_description=step_0_primitive_description)
        step_0.add_argument(name='inputs', argument_type=pipeline.ArgumentType.CONTAINER, data_reference='inputs.0')
        step_0.add_hyperparameter(name='primitive_1', argument_type=pipeline.ArgumentType.VALUE, data=primitive)
        step_0.add_hyperparameter(name='primitive_2', argument_type=pipeline.ArgumentType.VALUE, data=primitive)
        step_0.add_output('produce')
        pipeline_description.add_step(step_0)

        pipeline_description.add_output(name='output', data_reference='steps.0.produce')

        r = runtime.Runtime(pipeline_description)

        inputs = [container.List([1, 2, 3, 4, 5])]

        result = r.fit(inputs, return_values=['outputs.0'])
        result.check_success()

        self.assertEqual(len(result.values), 1)

        results = result.values['outputs.0']

        self.assertEqual(results, [
            2, 4, 6, 8, 10,
        ])

        result = r.produce(inputs, return_values=['outputs.0'])
        result.check_success()

        self.assertEqual(len(result.values), 1)

        results = result.values['outputs.0']

        self.assertEqual(results, [
            2, 4, 6, 8, 10,
        ])

        # Primitive should not be the same instance.
        self.assertIsNot(null_primitive, r.steps_state[0].hyperparams['primitive_1'])
        self.assertIsNot(r.steps_state[0].hyperparams['primitive_1'], r.steps_state[0].hyperparams['primitive_2'])

        pickle.dumps(r)


if __name__ == '__main__':
    unittest.main()
